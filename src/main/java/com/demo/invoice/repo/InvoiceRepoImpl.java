package com.demo.invoice.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import com.demo.invoice.data.InvoiceInfo;

public class InvoiceRepoImpl implements InvoiceCustomRepo  {
	@Autowired
    private MongoOperations mongoOps;

	@Override
	public void create(InvoiceInfo invInfo) {
		
		if(invInfo!=null){
			mongoOps.insert(invInfo);
		}
	}

}
