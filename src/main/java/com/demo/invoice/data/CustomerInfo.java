package com.demo.invoice.data;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class CustomerInfo {
	@Id
	private String id;
	private String email;
	private String mobileNo ;
	private String name;
}
