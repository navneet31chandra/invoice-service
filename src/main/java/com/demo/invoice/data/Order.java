package com.demo.invoice.data;

import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Order {
	@Id
	private String id;
	private CustomerInfo custInfo;
	private List<InventoryItem> invItems ;
	private InvoiceInfo invoiceInfo ;

}
