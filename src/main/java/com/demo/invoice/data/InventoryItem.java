package com.demo.invoice.data;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class InventoryItem {
	@Id
	private String InvId;
	private String Brand;
	private double Mrp;
	private double discount ;
	private Integer orderQuantity;

}
