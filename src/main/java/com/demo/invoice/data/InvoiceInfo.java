package com.demo.invoice.data;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
@Document(collection = "InvoiveInfo")
@Data
public class InvoiceInfo {
	@Id
	String id;
	@Indexed(background = true)
	String orderID;
	List<InventoryItem> invItems;
	private double cgstPerstange;
	private double sgstPerstange;
	private double igstPerstange;
	private double totalCgst;
	private double totalSgst;
	private double totalIgst;
	private double fullMrp ;
	private double discount ;
	private double payableAmount;
	

}
