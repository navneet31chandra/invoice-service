package com.demo.invoice.service;

import org.springframework.stereotype.Service;

import com.demo.invoice.data.Order;

@Service
public interface InvoiceService {

	  boolean createInvoice(Order order);
}
