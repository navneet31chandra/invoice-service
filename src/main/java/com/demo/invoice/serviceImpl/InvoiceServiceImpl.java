package com.demo.invoice.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.demo.invoice.data.InvoiceInfo;
import com.demo.invoice.data.Order;
import com.demo.invoice.repo.InvoiveRepo;
import com.demo.invoice.service.InvoiceService;
import com.demo.kafkaQ.constants.KafkaServiceTopic;
import com.demo.kafkaQ.service.KafkaQService;
@Service
@Component
public class InvoiceServiceImpl implements InvoiceService  {
	
	@Autowired
	private KafkaQService kafkaQService;
	@Autowired
	private InvoiveRepo invoiveRepo;

	@Override
	public boolean createInvoice(Order order) {
		try{
		InvoiceInfo invInfo = processInvoice(order) ;
		order.setInvoiceInfo(invInfo);
		invoiveRepo.insert(invInfo);
		/*
		//need to add rest api to update order in order servive 
		 * */
		kafkaQService.insert(order, KafkaServiceTopic.MAILTOPIC);
		return true;
		}catch(Exception e){
			kafkaQService.insert(order, KafkaServiceTopic.INVOICETOPIC);
			return false;
		}
	}

	private InvoiceInfo processInvoice(Order order) {
		
		return null;
	}

}
