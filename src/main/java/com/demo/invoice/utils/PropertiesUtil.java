package com.demo.invoice.utils;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class PropertiesUtil extends PropertyPlaceholderConfigurer {
	  private static Map<String, String> propertiesMap;

	  protected final static Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);

	  @Override
	  protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
	      throws BeansException {
	    super.processProperties(beanFactory, props);

	    propertiesMap = new HashMap<String, String>();
	    for (Object key : props.keySet()) {
	      String keyStr = key.toString();
	      propertiesMap.put(keyStr, resolvePlaceholder(keyStr, props));
	      logger.info("key = "+keyStr+" value = "+resolvePlaceholder(keyStr, props));
	    }
	  }

	  public static String getProperty(String name) {
	    return (String) propertiesMap.get(name);
	  }
	}
