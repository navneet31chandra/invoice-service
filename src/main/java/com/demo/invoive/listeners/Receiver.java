package com.demo.invoive.listeners;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

import com.demo.invoice.data.Order;
import com.demo.invoice.service.InvoiceService;
import com.demo.kafkaQ.constants.KafkaServiceTopic;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Receiver {
	private static final Logger LOGGER = LoggerFactory
            .getLogger(Receiver.class);
	 private CountDownLatch latch = new CountDownLatch(1);
	 
	 @Autowired
	 private InvoiceService invoiceService;
	
	 @KafkaListener(containerFactory = "kafkaListenerContainerFactory",topics=KafkaServiceTopic.INVOICETOPIC)
	    public void receiveInvoive(ConsumerRecord<String, Object> consumerRecord) throws IOException {
	        if(consumerRecord != null) {
	            String message = consumerRecord.value().toString();
	            LOGGER.info("Received Message at offset "+ consumerRecord.offset() + "_" + consumerRecord.partition() +
	                    "_" + consumerRecord.topic());
	            ObjectMapper mapper = new ObjectMapper();
	            Order order = mapper.readValue(message, Order.class);
	            invoiceService.createInvoice(order);
	            LOGGER.info("Received object " + order.getId() );
	            
	        }
	        latch.countDown();
	    }

}
